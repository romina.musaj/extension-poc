import * as React from "react";
import { fixedColumns, tableItemsNoIcons } from "./TableData";

import { Card } from "azure-devops-ui/Card";
import { Table } from "azure-devops-ui/Table";

export default class HubTable extends React.Component {
    public render(): JSX.Element {
        return (
            <Card className="flex-grow bolt-table-card" contentProps={{ contentPadding: false }}>
                <Table columns={fixedColumns} itemProvider={tableItemsNoIcons} role="table" />
            </Card>
        );
    }
}

import * as React from "react";
import { ObservableValue } from "azure-devops-ui/Core/Observable";
import { MenuItemType } from "azure-devops-ui/Menu";
import { Status, Statuses, StatusSize } from "azure-devops-ui/Status";
import {
    ColumnFill,
    ColumnMore,
    ISimpleTableCell,
    renderSimpleCell,
    TableColumnLayout
} from "azure-devops-ui/Table";
import { css } from "azure-devops-ui/Util";
import { ArrayItemProvider } from "azure-devops-ui/Utilities/Provider";

export interface ITableItem extends ISimpleTableCell {
    name: string;
    hours: number;
}


export const fixedColumns = [
    {
        columnLayout: TableColumnLayout.singleLinePrefix,
        id: "name",
        name: "Name",
        readonly: true,
        renderCell: renderSimpleCell,
        width: new ObservableValue(200)
    },
    {
        id: "hours",
        name: "Hours",
        readonly: true,
        renderCell: renderSimpleCell,
        width: new ObservableValue(100)
    },
    ColumnFill
];


export function onSizeMore(event: MouseEvent, index: number, width: number) {
    (moreColumns[index].width as ObservableValue<number>).value = width;
}

export const moreColumns = [
    {
        id: "name",
        minWidth: 50,
        name: "Name",
        onSize: onSizeMore,
        renderCell: renderSimpleCell,
        width: new ObservableValue(200)
    },
    {
        id: "hours",
        maxWidth: 300,
        name: "Hours",
        onSize: onSizeMore,
        renderCell: renderSimpleCell,
        width: new ObservableValue(100)
    },
    ColumnFill,
    new ColumnMore(() => {
        return {
            id: "sub-menu",
            items: [
                { id: "submenu-one", text: "SubMenuItem 1" },
                { id: "submenu-two", text: "SubMenuItem 2" },
                { id: "divider", itemType: MenuItemType.Divider },
                { id: "submenu-three", checked: true, readonly: true, text: "SubMenuItem 3" },
                { id: "submenu-four", disabled: true, text: "SubMenuItem 4" }
            ]
        };
    })
];

export const renderStatus = (className?: string) => {
    return (
        <Status
            {...Statuses.Success}
            ariaLabel="Success"
            className={css(className, "bolt-table-status-icon")}
            size={StatusSize.s}
        />
    );
};

export const rawTableItems: ITableItem[] = [
    {
        hours: 50,
        name: "Task 1"
    },
    {
        hours: 49,
        name: "Task 2"
    },
    {
        hours: 18,
        name: "Task 3"
    }
];

export const tableItems = new ArrayItemProvider<ITableItem>(rawTableItems);


export const tableItemsNoIcons = new ArrayItemProvider<ITableItem>(
    rawTableItems.map((item: ITableItem) => {
        const newItem = Object.assign({}, item);
        return newItem;
    })
);
import {
  IWorkItemChangedArgs,
  IWorkItemFieldChangedArgs,
  IWorkItemLoadedArgs
} from "azure-devops-extension-api/WorkItemTracking";
import * as SDK from "azure-devops-extension-sdk";
import * as React from "react";
import { Card } from "azure-devops-ui/Card";
import { TextField, TextFieldWidth } from "azure-devops-ui/TextField";
import { Button } from "azure-devops-ui/Button";
import { ObservableValue } from "azure-devops-ui/Core/Observable";
import { FormItem } from "azure-devops-ui/FormItem";
import { Toast } from "azure-devops-ui/Toast";
import { Page } from "azure-devops-ui/Page";

import { showRootComponent } from "../../Common";

const hourObservable = new ObservableValue<string>("");

interface IToastExampleState {
  isToastVisible: boolean;
  isToastFadingOut: boolean;
  eventContent?: any;
}

class WorkItemFormPageComponent extends React.Component<{}, IToastExampleState> {

  private toastRef: React.RefObject<Toast> = React.createRef<Toast>();


  constructor(props: {}) {
    super(props);
    this.state = {
      isToastVisible: false,
      isToastFadingOut: false
    };
  }

  public componentDidMount() {
    SDK.init().then(() => {
      this.registerEvents();
    });
  }

  public render(): JSX.Element {
    const { isToastVisible, isToastFadingOut } = this.state;
    return (
      <Page className="absolute-fill padding-left-16">
        <Card
          className="flex-grow"
          headerClassName="separator-line-bottom"
          titleProps={{ text: "Register work item hours" }}
          contentProps={{ className: "justify-center" }}
        >
          <div className="flex-column  justify-center">
            <FormItem
              className="margin-bottom-4"
              label="Hours: "
            >
              <TextField
                value={hourObservable}
                onChange={(_, newValue) => (hourObservable.value = newValue)}
                placeholder="Enter hours"
                width={TextFieldWidth.standard}
              />
            </FormItem>
            <Button
              text="Save"
              disabled={isToastFadingOut}
              primary={true}
              onClick={this.onButtonClick}
            />
          </div>
          {isToastVisible && (
            <Toast
              ref={this.toastRef}
              message="The hours were successfully logged!"
            />
          )}
        </Card>

      </Page>


    );
  }

  private onButtonClick = () => {
    if (this.state.isToastFadingOut) {
      return;
    }

    if (this.state.isToastVisible && this.toastRef.current) {
      this.setState({ isToastFadingOut: true });
      this.toastRef.current.fadeOut().promise.then(() => {
        this.setState({ isToastVisible: false, isToastFadingOut: false });
      });
    } else {
      this.setState({ isToastVisible: true });
    }
  };

  private registerEvents() {
    SDK.register(SDK.getContributionId(), () => {
      return {
        // Called when the active work item is modified
        onFieldChanged: (args: IWorkItemFieldChangedArgs) => {
          this.setState({
            eventContent: `onFieldChanged - ${JSON.stringify(args)}`
          });
        },

        // Called when a new work item is being loaded in the UI
        onLoaded: (args: IWorkItemLoadedArgs) => {
          this.setState({
            eventContent: `onLoaded - ${JSON.stringify(args)}`
          });
        },

        // Called when the active work item is being unloaded in the UI
        onUnloaded: (args: IWorkItemChangedArgs) => {
          this.setState({
            eventContent: `onUnloaded - ${JSON.stringify(args)}`
          });
        },

        // Called after the work item has been saved
        onSaved: (args: IWorkItemChangedArgs) => {
          this.setState({
            eventContent: `onSaved - ${JSON.stringify(args)}`
          });
        },

        // Called when the work item is reset to its unmodified state (undo)
        onReset: (args: IWorkItemChangedArgs) => {
          this.setState({
            eventContent: `onReset - ${JSON.stringify(args)}`
          });
        },

        // Called when the work item has been refreshed from the server
        onRefreshed: (args: IWorkItemChangedArgs) => {
          this.setState({
            eventContent: `onRefreshed - ${JSON.stringify(args)}`
          });
        }
      };
    });
  }




}

showRootComponent(<WorkItemFormPageComponent />);
